use toml::Value;

fn main() {
    let value = std::fs::read_to_string("src/classes.toml")
        .unwrap()
        .parse::<Value>().unwrap();
    println!("TOML: {:?}", value);
    let t = value.as_table().unwrap();
    for key in t.keys() {
        println!("key: {:?}", key);
        let cpp: &str = t[key]["cpp"].as_str().unwrap();
        let rust: &str = t[key]["rust"].as_str().unwrap();
        println!("cpp: {}", cpp);
        println!("rust: {}", rust);
    }
}
