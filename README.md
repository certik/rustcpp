# Rust C++ Rosetta Stone

## Other similar projects:

Python Fortran Rosetta Stone:
https://www.fortran90.org/src/rosetta.html

MATLAB–Python–Julia cheatsheet:
https://cheatsheets.quantecon.org/index.html

From numpy to xtensor:
https://xtensor.readthedocs.io/en/latest/numpy.html
