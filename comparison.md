Oberon

    PROCEDURE NewObj(VAR obj: Object; class: INTEGER);
        VAR new, x: Object;
    BEGIN
        x := topScope;
        WHILE (x.next # NIL) & (x.next.name # id) DO x := x.next END ;
        IF x.next = NIL THEN
            NEW(new); new.name := id; new.class := class; new.next := NIL;
            x.next := new; obj := new
        ELSE obj := x.next; Mark("multiple declaration")
        END
    END NewObj;

Oberon-R (Oberon semantics with Rust like syntax)

    fn NewObj(var obj: Object, class: INTEGER)
        var new, x: Object;
    {
        x = topScope;
        while (x.next != null) && (x.next.name != id) {
            x = x.next;
        }
        if x.next == null {
            NEW(new); new.name = id; new.class = class; new.next = null;
            x.next = new; obj = new;
        } else {
            obj = x.next; Mark("multiple declaration");
        }
    }
